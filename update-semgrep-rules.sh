#!/bin/bash

echo "Updating Semgrep rules"

# Remove any existing rules
rm -rf semgrep-rules/*
# Clone rules repository and extract JavaScript/TypeScript rules
git clone https://github.com/returntocorp/semgrep-rules.git ./.tmp/semgrep-rules
# Change directory to perform git commands on that repository
cd ./.tmp/semgrep-rules || exit 1

git checkout -d "$SEMGREP_RULES_REF"

# Move applicable rules and license
mv go javascript typescript LICENSE ../../semgrep-rules/
# Delete git repository
cd ../../ && rm -rf .tmp

# Find non-security rules and replace YAML extensions with regex covering all
# extensions, which are the example files for the rules (many formats).
NON_SECURITY_RULES_EXAMPLES=$( \
  find ./semgrep-rules -type f \( -name "*.yaml" -o -name "*.yml" \) \
    \! -exec grep -q "category: security" {} \; -print | \
  sed -E 's/\.ya?ml/\\.[a-z.]\+/g')

# Delete non-security rules example files
for file in $NON_SECURITY_RULES_EXAMPLES; do
  find ./semgrep-rules -type f -regex "$file" -delete
done

# Loop through rule list in file gitlab-rule-duplicates.txt and delete those
# files with any extension (rules and examples) in favor of the GitLab rules
while read -r line; do
  find ./semgrep-rules -type f -regex "$line\.[a-z.]+" -delete
done < ./gitlab-rule-duplicates.txt

# Delete any empty directories (only contained non-security or depreacated rules)
find ./semgrep-rules -type d -empty -delete
