# GitLab Semgrep Plus

A container image to run [Semgrep](https://semgrep.dev/), one of the
[GitLab SAST analyzers](https://docs.gitlab.com/ee/user/application_security/sast/),
with an augmented set of rules for Node.js/JavaScript/TypeScript and Go.

## Semgrep updates

This container image is based on original
[GitLab `semgrep` image](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep),
leveraging the existing wrapper to execute the scan and provide a report in
the appropriate format. The GitLab image only includes GitLab-provided custom
rules, for example custom implementations of rules from
[`eslint-plugin-security`](https://www.npmjs.com/package/eslint-plugin-security)
for JavaScript/TypeScript and [`gosec`](https://github.com/securego/gosec) for
Go. There are numerous other rules available in the
[`semgrep-rules` repository](https://github.com/returntocorp/semgrep-rules).
For this image the `security` rules in the following directories are also
included: `contrib/nodejsscan/`, `go/`, `javascript/`, and `typescript/` (~250
additional rules for JavaScript/TypeScript and ~50 rules for Go). Using only
the `security` rules keeps this a pure SAST job. The existing rules for
`best-practices` and `correctness` are generally better handled by other tools.
The specific rules and their example files can be found in the
`semgrep-rules` directory. The `semgrep-rules` that are duplicates of GitLab
rules are removed to avoid duplicate findings.

## Usage

The following example shows how to use this image in to execute the
`semgrep-sast` job. Since it's based on the original GitLab `semgrep` image
and the rules are added to the existing rules directory the script is the same,
only the `image` has to be changed. The original GitLab SAST template provides
the variable `SAST_ANALYZER_IMAGE` to override the image as shown below.

```yml
include:
  - template: SAST.gitlab-ci.yml

semgrep-sast:
  variables:
    SAST_ANALYZER_IMAGE: registry.gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus:latest
```

## GitLab Semgrep Plus container images

All available Docker image tags are available in the gitlab-ci-utils/gitlab-semgrep-plus
repository at <https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/container_registry>.

The following tag conventions are used:

- `latest`: The latest build of the image.
- `X.Y.Z`: Versioned releases of the image. Details on each release can be
  found on the [Releases](https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus/-/releases)
  page.

_Note: the image tags using the previous, pre-1.0.0 versioning have been
removed to avoid confusion._

## License

The original GitLab SAST image and this project, including the built container
image, is licensed under the [MIT license](LICENSE). The Semgrep Rules are
licensed under the
[LGPL 2.1 license](https://www.tldrlegal.com/license/gnu-lesser-general-public-license-v2-1-lgpl-2-1).
This license is included with the rules in the `/rules/semgrep-rules/`
directory in the image. The source code of the rules, with all examples, is
available from the `semgrep-rules` directory in this repository and in the
[`semgrep-rules` repository](https://github.com/returntocorp/semgrep-rules).
