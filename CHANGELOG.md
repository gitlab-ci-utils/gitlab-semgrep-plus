# Changelog

## v9.2.0 (2025-03-07)

### Changed

- Updated to GitLab [`semgrep:v5.28.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v9.1.4 (2025-01-31)

### Fixed

- Revert `semgrep-rules` to previous version prior to license change, which is
  now incompatible with this distribution. For more information, see
  [this post](https://semgrep.dev/blog/2024/important-updates-to-semgrep-oss/).
  - There are no functional rule changes.
- Updated to GitLab [`semgrep:v5.26.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v9.1.3 (2024-12-14)

### Fixed

- Updated to GitLab [`semgrep:v5.25.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).
- Update license information for numerous `semgrep-rules` rules.
- Update rule [`tainted-sql-string`](semgrep-rules/javascript/aws-lambda/security/tainted-sql-string.yaml)
  to `LOW` confidence.

## v9.1.2 (2024-12-06)

### Fixed

- Updated to GitLab [`semgrep:v5.24.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v9.1.1 (2024-12-02)

### Fixed

- Updated TypeScript rule [`react-insecure-request`](semgrep-rules/typescript/react/security/react-insecure-request.yaml)
  to improve accuracy.

## v9.1.0 (2024-11-29)

### Changed

- Updated to GitLab [`semgrep:v5.23.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v9.0.3 (2024-11-21)

### Fixed

- Updated JavaScript rule [`express-xml2json-xxe-event`](semgrep-rules/javascript/express/security/audit/express-xml2json-xxe-event.js)
  to improve accuracy.

## v9.0.2 (2024-11-14)

### Fixed

- Updated to GitLab [`semgrep:v5.20.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).
- Updated JavaScript rule [`sequelize-raw-query`](semgrep-rules/javascript/sequelize/security/audit/sequelize-raw-query.yaml)
  to improve accuracy.

## v9.0.1 (2024-11-11)

### Fixed

- Updated to GitLab [`semgrep:v5.20.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v9.0.0 (2024-10-25)

### Changed

- BREAKING: Added Go rules [`reverseproxy-director`](semgrep-rules\go\lang\security\reverseproxy-director.yaml)
  and [`shared-url-struct-mutation`](semgrep-rules\go\lang\security\shared-url-struct-mutation.yaml).
  (See rule [PR](https://github.com/semgrep/semgrep-rules/pull/3486)).
- BREAKING: Added JavaScript rules [`aead-no-final`](semgrep-rules\javascript\node-crypto\security\aead-no-final.yaml),
  [`create-de-cipher-no-iv`](semgrep-rules\javascript\node-crypto\security\create-de-cipher-no-iv.yaml),
  [`gcm-no-tag-length`](semgrep-rules\javascript\node-crypto\security\gcm-no-tag-length.yaml).
  (See rule [PR](https://github.com/semgrep/semgrep-rules/pull/3357)).
- BREAKING: Removed redundant JavaScipt rule `var-in-href`.
  (See rule [PR](https://github.com/semgrep/semgrep-rules/pull/3349)).

### Fixed

- Updated to GitLab [`semgrep:v5.19.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.6.4 (2024-10-20)

### Fixed

- Updated to GitLab [`semgrep:v5.18.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases),
  which includes several rule fixes to reduce false positives.
- Fixed CI pipeline to add correct OCI image `annotations` for this project.
  (#19)

## v8.6.3 (2024-09-29)

### Fixed

- Updated to GitLab [`semgrep:v5.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.6.2 (2024-09-20)

### Changed

- Updated to GitLab [`semgrep:v5.14.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.6.1 (2024-09-19)

### Changed

- Updated to GitLab [`semgrep:v5.14.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.6.0 (2024-09-13)

### Changed

- Updated to GitLab [`semgrep:v5.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases),
  which adds `--max-target-bytes` and `--timeout` CLI options.

## v8.5.7 (2024-08-30)

### Fixed

- Updated to GitLab [`semgrep:v5.12.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.5.6 (2024-08-15)

### Fixed

- Updated to GitLab [`semgrep:v5.11.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.5.5 (2024-08-14)

### Fixed

- Updated to GitLab [`semgrep:v5.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.5.4 (2024-08-07)

### Fixed

- Updated to GitLab [`semgrep:v5.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.5.3 (2024-07-24)

### Fixed

- Updated to GitLab [`semgrep:v5.9.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.5.2 (2024-07-19)

### Fixed

- Updated to GitLab [`semgrep:v5.8.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases),
  which removes support for `*.html` files since experimental and
  [failing in some cases](https://gitlab.com/gitlab-org/gitlab/-/issues/375099#note_1998781865).

## v8.5.1 (2024-07-18)

### Fixed

- Updated to GitLab [`semgrep:v5.7.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.5.0 (2024-07-17)

### Changed

- Updated to GitLab [`semgrep:v5.6.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.4.0 (2024-07-12)

### Changed

- Updated to GitLab [`semgrep:v5.5.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases),
  which adds support for `*.html` files.

## v8.3.1 (2024-07-11)

### Fixed

- Updated to GitLab [`semgrep:v5.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.3.0 (2024-07-08)

### Changed

- Updated to GitLab [`semgrep:v5.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases),
  which adds support for `*.cjs` and `*.mjs` files.

### Fixed

- Fixed `subcategory` for rules
  [`semgrep-rules\go\lang\security\audit\crypto\missing-ssl-minversion.yaml`](semgrep-rules\go\lang\security\audit\crypto\missing-ssl-minversion.yaml) and
  [`semgrep-rules\javascript\intercom\security\audit\intercom-settings-user-identifier-without-user-hash.yaml`](semgrep-rules\javascript\intercom\security\audit\intercom-settings-user-identifier-without-user-hash.yaml).

## v8.2.3 (2024-06-25)

### Fixed

- Updated to GitLab [`semgrep:v5.3.3`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.2.2 (2024-06-24)

### Fixed

- Updated to GitLab [`semgrep:v5.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.2.1 (2024-06-21)

### Fixed

- Updated to GitLab [`semgrep:v5.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).
- Updated rule
  [`semgrep-rules/javascript/express/security/injection/tainted-sql-string.yaml`](semgrep-rules/javascript/express/security/injection/tainted-sql-string.yaml)
  to improve accuracy.

## v8.2.0 (2024-06-11)

### Changed

- Updated to GitLab [`semgrep:v5.3.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.1.3 (2024-06-04)

### Fixed

- Updated to GitLab [`semgrep:v5.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.1.2 (2024-05-27)

### Fixed

- Updated to GitLab [`semgrep:v5.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).

## v8.1.1 (2024-05-23)

### Fixed

- Updated to GitLab [`semgrep:v5.2.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).
- Corrected link to the source project in the `semgrep-rules/LICENSE` file.

### Miscellaneous

- Updated Renovate presets to v1.1.0.

## v8.1.0 (2024-05-13)

### Changed

- Updated to GitLab [`semgrep:v5.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases),
  which includes adding support for Rust. See the release notes for the full
  list of rule changes.

## v8.0.1 (2024-05-09)

### Fixed

- Updated detection for rule `semgrep-rules/javascript/express/security/audit/xss/direct-response-write.yaml`.

## v8.0.0 (2024-05-08)

### Changed

- BREAKING: Updated to GitLab [`semgrep:v5.0.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases).
  See the release notes for the full list of rule changes.
- BREAKING: Added rule `semgrep-rules/go/gorilla/security/audit/session-cookie-samesitenone.yaml`.

### Fixed

- Updated metadata for rule `semgrep-rules/javascript/express/security/injection/tainted-sql-string.yaml`.

## v7.0.1 (2024-05-07)

### Fixed

- Updated to GitLab [`semgrep:v4.18.2`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.18.2).
  See the release notes for the full list of rule changes.

## v7.0.0 (2024-05-02)

### Changed

- BREAKING: Updated from GitLab `semgrep:v4.13.5` to GitLab `semgrep:v4.18.1`
  which includes numerous changes for GitLab 17.0. See the
  [release notes](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases)
  for the full list of changes. Significant changes include:
  - `v4.14.0` incorporated the NodeJS Scan rules. These were previously
    removed in v4.0.0 in favor of the `nodejs-scan-sast` job from the GitLab
    `SAST` template, which is being removed in GitLab 17.0.
  - `v4.15.0` update the C rules for C++ (based on Flawfinder rules).
  - `v4.16.0` added MobSF and Brakeman rules.
  - `v4.17.0` added Ruby support.
  - `v4.18.0` added Kotlin support.
- BREAKING: Added rule `semgrep-rules/javascript/jwt-simple/security/jwt-simple-noverify.yaml`.

### Fixed

- Improved detection for rule `semgrep-rules/javascript/express/security/injection/tainted-sql-string.yaml`.

## v6.2.1 (2024-04-12)

### Fixed

- Updated to GitLab [`semgrep:v4.13.5`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.13.5).
  See the release notes for the full list of rule changes.

## v6.2.0 (2024-04-11)

### Changed

- Updated to GitLab [`semgrep:v4.13.4`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.13.4).
  See the release notes for the full list of rule changes.
  - Moved from GitLab rule `detect-child-process`, which was removed, to
    Semgrep rule `detect-child-process`, which was previously removed as a
    duplicate. These are slightly different implementations, so could fail
    in different cases.

## v6.1.2 (2024-03-25)

### Fixed

- Updated to GitLab [`semgrep:v4.13.3`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.13.3).
  See the release notes for the full list of rule changes.

## v6.1.1 (2024-03-14)

### Changed

- Updated to GitLab [`semgrep:v4.13.2`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.13.2).
  See the release notes for the full list of rule changes.

## v6.1.0 (2024-03-13)

### Changed

- Updated to GitLab [`semgrep:v4.13.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.13.1).
  See the release notes for the full list of rule changes.

## v6.0.0 (2024-02-26)

- BREAKING: Removed 21 deprecated Semgrep rules. See complete details
  [here](https://github.com/semgrep/semgrep-rules/pull/3311).

## v5.2.3 (2024-02-22)

### Fixed

- Updated to GitLab [`semgrep:v4.12.2`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.12.2).
  See the release notes for the full list of rule changes.
- Improved accuracy for rule
  [`semgrep-rules/go/jwt-go/security/jwt-none-alg.yaml`](semgrep-rules/go/jwt-go/security/jwt-none-alg.yaml).

## v5.2.2 (2024-02-19)

### Changed

- Updated to GitLab [`semgrep:v4.12.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.12.1).
  See the release notes for the full list of rule changes.

### Miscellaneous

- Update Renovate config to use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config/-/blob/main/presets/docker-pin-digests.json).
  (#17)

## v5.2.1 (2024-02-09)

### Fixed

- Improved coverage for rule
  [`go/lang/security/audit/database/string-formatted-query.yaml`](semgrep-rules/go/lang/security/audit/database/string-formatted-query.yaml).

## v5.2.0 (2024-02-08)

### Changed

- Updated to GitLab [`semgrep:v4.12.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.12.0).
  See the release notes for the full list of rule changes.

### Miscellaneous

- Changed rule update script to use SSH key instead of token to push changes.
  (#16)

## v5.1.0 (2024-01-31)

### Changed

- Updated to GitLab [`semgrep:v4.11.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.11.0).
  See the release notes for the full list of rule changes.

## v5.0.0 (2024-01-23)

### Changed

- BREAKING: Added rule `go/lang/security/injection/open-redirect.yaml`.
- Updated to GitLab [`semgrep:v4.10.4`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.10.4).
  - See the [release note](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases)
    and [full list of changes](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/compare/v4.10.1...v4.10.4?from_project_id=24075172&straight=false).

### Fixed

- Update Go rule `go/lang/security/injection/tainted-url-host.yaml` to reduce
  false positive results.

## v4.0.0 (2023-12-29)

### Changed

- BREAKING: Removed all `nodejsscan` rules from `semgrep-rules` repository
  (per [this PR](https://github.com/semgrep/semgrep-rules/pull/3250)).
  These rules were unmaintained and deviated from the official `nodejsscan`
  rules. The current `nodejsscan` analyzer is included in the GitLab SAST
  template, so those checks are still performed if using the template. (#15)
  - Any `nosemgrep` references to these rules may generate warnings and
    should be removed.

## v3.3.0 (2023-12-20)

### Changed

- Update to GitLab [`semgrep:v4.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.10.1).
  Given the rule enhancements, this was considered a minor change.

## v3.2.0 (2023-11-28)

### Changed

- Update to GitLab [`semgrep:v4.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.10.0),
  which updates rule `eslint.detect-non-literal-regexp` to filter out RegExp
  literals and match non-constructor RegExp() function calls.

## v3.1.0 (2023-11-27)

### Changed

- Update to GitLab [`semgrep:v4.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.9.1),
  which includes update to [Semgrep v1.50.0](https://github.com/semgrep/semgrep/releases/tag/v1.50.0).
- Update Semgrep rule [`jwt-hardcode`](semgrep-rules/javascript/jsonwebtoken/security/jwt-hardcode.yaml)
  to improve accuracy.

## v3.0.0 (2023-11-13)

### Changed

- BREAKING: Updated to GitLab `semgrep` v4.8.0, which removed rules
  `eslint.detect-no-csrf-before-method-override` and
  `eslint.react-missing-noopener`. See
  [`sast-rules v1.3.44`](https://gitlab.com/gitlab-org/security-products/sast-rules/-/releases/v1.3.44)
  for details.

### Fixed

- Updated Semgrep rule `hardcoded-jwt-secret` to improve accuracy.

## v2.0.0 (2023-10-28)

### Changed

- BREAKING: Added [`semgrep-rules`](https://github.com/returntocorp/semgrep-rules)
  for Go, excluding those that are duplicates of GitLab rules. (#13)

## v1.1.0 (2023-10-26)

### Changed

- Updated `semgrep` to [v1.45.0](https://github.com/returntocorp/semgrep/releases/tag/v1.45.0).
- Update to GitLab [`semgrep:v4.5.0`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.5.0).

## v1.0.3 (2023-10-13)

### Fixed

- Upgraded to GitLab [`semgrep:4.4.15`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.4.15).
- Fix Semgrep rule `cors-regex-wildcard` pattern matching.

## v1.0.2 (2023-09-27)

### Fixed

- Upgraded to GitLab [`semgrep:4.4.14`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.4.14),
  which includes update to Semgrep [v1.41.0](https://github.com/returntocorp/semgrep/releases/tag/v1.41.0).

## v1.0.1 (2023-09-26)

### Fixed

- Updated to GitLab [`semgrep:4.4.13`](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v4.4.13).
- Updated to latest Semgrep rules, which updated some reference links.
- Fixed issue with Semgrep rules update always reporting changes. (#12)

## v1.0.0 (2023-09-04)

### Changed

- BREAKING: Updated repository to include applicable `semgrep` rules to provide
  meaningful insight into rule changes. Updated project to automatically update
  rules when `semgrep-rules` git ref is update. Project is now released with
  [semantic versioning](https://semver.org/). (#7, #9)
  - Changes to `semgrep-rules` are tracked by git ref since the project does
    not release specific versions.
  - The image tags using the previous, pre-1.0.0 versioning have been removed
    to avoid confusion.
- BREAKING: Removed Semgrep rules that are duplicates of GitLab rules. (#4)
  - Both include a subset of the `eslint-plugin-security` rules. Previously
    both sets of rules were included, so in some cases duplicate rules were
    flagged. The Semgrep rules are removed to make this image backwards
    compatible with GitLab's image, not based on the specific implementations.
  - The complete list of removed rules is available in
    `gitlab-rule-duplicates.txt`. The GitLab rules can be found
    [here](https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/dist/eslint.yml).
    Note that Semgrep will report a warning if there were inline suppressions
    for any of these rules since they are no longer be found.
- BREAKING: Semgrep rule updates:
  - Added rule [`detect-redos`](./semgrep-rules/javascript/lang/security/audit/detect-redos.yaml)
  - Update rule [`detect-insecure-websocket`](./semgrep-rules/javascript/lang/security/detect-insecure-websocket.yaml)
    to eliminate some false positive cases.
  - Deprecated rule [`harden-dompurify-usage`](./semgrep-rules/javascript/dompurify.yaml)

### Miscellaneous

- Updated `renovate` config to track GitLab semgrep image tag/digest as well
  as `semgrep-rules` git ref. (#3, #8)

## v0.5.0 (2023-04-20)

Initial release
