ARG SEMGREP_IMAGE_TAG=latest

# Use latest since all generic capabilities.
# hadolint ignore=DL3007
FROM alpine:latest as rules

COPY semgrep-rules/ /semgrep-rules/

RUN \
  # Delete all files except rules (YAML) and the LICENSE, which are all example files
  find /semgrep-rules -type f \! \( -name "*.yaml" -o -name "*.yml" -name "LICENSE" \) -delete && \
  # Delete any empty directories (did not contain security rules)
  find /semgrep-rules -type d -empty -delete

FROM registry.gitlab.com/gitlab-org/security-products/analyzers/semgrep:$SEMGREP_IMAGE_TAG

COPY --from=rules /semgrep-rules /rules/semgrep-rules/

LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus"
LABEL org.opencontainers.image.title="gitlab-semgrep-plus"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/gitlab-semgrep-plus"
